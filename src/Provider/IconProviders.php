<?php

namespace Bolt\Extension\Adhehidayat\FieldsIcon\Provider;

use Bolt\Extension\AdheHidayat\FieldsIcons\Field\IconFields;
use Bolt\Storage\FieldManager;
use Silex\Application;
use Silex\ServiceProviderInterface;

class IconProviders implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['storage.typemap'] = array_merge($app['storage.typemap'], [
            'selecticon' => IconFields::class
        ]);
        $app['storage.field_manager'] = $app->share($app->extend('storage.field_manager', function (FieldManager $manager) {
            $manager->addFieldType('selecticon', new IconFields());
            return $manager;
        }));
    }

    public function boot(Application $app)
    {

    }

}
