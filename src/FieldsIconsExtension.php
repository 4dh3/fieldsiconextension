<?php

namespace Bolt\Extension\AdheHidayat\FieldsIcons;

use Bolt\Asset\File\JavaScript;
use Bolt\Asset\File\Stylesheet;
use Bolt\Controller\Zone;
use Bolt\Extension\SimpleExtension;
use Bolt\Extension\AdheHidayat\FieldsIcons\Field\IconFields;

/**
 * ExtensionName extension class.
 *
 * @author Adhehidayat <adhe.hidayat89@gmail.com>
 */
class FieldsIconsExtension extends SimpleExtension
{
    public function registerFields()
    {
        return [
            new IconFields(),
        ];
    }

    protected function registerTwigPaths()
    {
        return [
            'templates' => ['position' => 'prepend', 'namespace' => 'bolt']
        ];
    }

    protected function registerAssets()
    {
        $css = new Stylesheet();
        $css
            ->setFileName('extension.css')
            ->setZone(Zone::BACKEND)
        ;

        $javascript = new JavaScript();
        $javascript
            ->setFileName('extension.js')
            ->setZone(Zone::BACKEND)
        ;

        $javascript
            ->setFileName('dist/js/bootstrap-select.js')
            ->setZone(Zone::BACKEND)
        ;
        return [
            $css,
            $javascript
        ];

    }


}
